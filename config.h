/* user and group to drop privileges to */
static const char *user  = "erenoit";
static const char *group = "erenoit";

static const char *colorname[NUMCOLS] = {
	[INIT]   = "#1a2332",     /* after initialization */
	[INPUT]  = "#5cb2ff",   /* during input */
	[FAILED] = "#ff3e3e",   /* wrong password */
	[CAPS]   = "#ffaf00",   /* CapsLock on */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* allow control key to trigger fail on clear */
static const int controlkeyclear = 1;

/* time in seconds before the monitor shuts down */
static const int monitortime = 5;

/* foreground image patch */
static const char *imgpath          = "/home/erenoit/.suckless/slock/image.xpm";
static const int imgbordersize      = 5;
static const int showimgonlyatstart = 0;

/* image size */
static const int imgwidth           = 1920 - 2*imgbordersize;
static const int imgheight          = 1080 - 2*imgbordersize;
static const int imgoffsetx         = imgbordersize;
static const int imgoffsety         = imgbordersize;


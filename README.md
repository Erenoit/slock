# Erenoit's slock
This is my fork of Suckless's slock. Original git repo is [here](https://git.suckless.org/slock/)

## What is slock?
slock is simple screen locker utility for X.


## Requirements
In order to build slock you need the Xlib header files.


## Installation
Edit config.mk to match your local setup (slock is installed into the `/usr/local` namespace by default).

Afterwards, enter the following command to build and install slock (if necessary as root):

```sh
make clean install
```

### Enabling auto lock on sleep and suspend
Create `slock@.service` file in `/etc/systemd/system/`:

```slock@.service
[Unit]
Description=Lock X session using slock for user %i
Before=sleep.target
Before=suspend.target

[Service]
User=%i
Type=Simple
Environment=DISPLAY=:0
ExecStartPre=/usr/bin/xset dpms force suspend
ExecStart=/usr/local/bin/slock
TimeoutSec=infinity

[Install]
WantedBy=sleep.target
WantedBy=suspend.target
```

Then run this command to enable the auto lock:

```sh
systemctl enable slock@$USERNAME.sevice
```

### Extra Security Options
If want extra security, to make sure a locked screen can not be bypassed by switching VTs or killing the X server with `Ctrl+Alt+Backspace`, you can disable both in xorg.conf(5) for maximum security:

```xorg.conf
Section "ServerFlags"
    Option "DontVTSwitch" "True"
    Option "DontZap"      "True"
EndSection
```

But, if you need to switch to a different VT or kill X server to fix something, you won't be able to do. It may be really annoying.

## Configuration
The configuration of slock is done by creating a custom config.h and (re)compiling the source code.

## Patches
All the patches are included in `patches/` folder.
- [capscollor](https://tools.suckless.org/slock/patches/capscolor/)
- [control clear](https://tools.suckless.org/slock/patches/control-clear/)
- [dmps](https://tools.suckless.org/slock/patches/dpms/)
- [foreground image](https://tools.suckless.org/slock/patches/foreground-image/)
- [mediakeys](https://tools.suckless.org/slock/patches/mediakeys/)
- [terminalkeys](https://tools.suckless.org/slock/patches/terminalkeys/)
